﻿using System;
using System.Threading.Tasks;
using kusema.domain.Entities;

namespace kusema.domain.Services
{
    public interface ISubscriptionService
    {
        Task<ISubscription> Subscribe(User user, ILession lession);
        Task Unsubscribe(ISubscription subscription);
    }
}
