﻿using System.Threading.Tasks;
using kusema.domain.Entities;

namespace kusema.domain.Services
{
    public interface IPaymentService
    {
        Task RequestPayment(PaymentRequest paymentRequest);
    }
}
